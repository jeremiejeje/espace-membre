-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 19 Janvier 2017 à 11:45
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `espace_membre`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `confirmation_token` varchar(60) DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `reset_token` varchar(60) DEFAULT NULL,
  `reset_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `confirmation_token`, `confirmed_at`, `reset_token`, `reset_at`) VALUES
(52, 'jejems95', 'jeje.a@gmail.com', '$2y$10$9ERarCAv0oR3n8WY9uhb8OMTfcBZ.uP9vJmTZ1pm2GwpJ5WJ83kOm', NULL, '2017-01-02 10:08:14', 'MaIFCXl3pti3QBNfjJLswijMSKUvKT5Mm6xziEs1oge35NXtijvCN8jn1N22', '2017-01-08 00:26:31'),
(51, 'jejems', 'abtjeremie@gmail.com', '$2y$10$mRjGNOQOR4JNGABCC14AGe25jWrWxDnIreuE5fUg6qwr0N.GtSNFS', NULL, '2017-01-02 09:50:50', '0', NULL),
(53, 'qsd', 'qsd@fg.fr', '$2y$10$yDU7lo.WykwsnS8LjI8BfOzmKE0V6iLBc70dtTZtSC.oxNB73fAye', NULL, '2017-01-02 12:38:20', NULL, NULL),
(54, 'qsdd', 'qsd@fg.frs', '$2y$10$h.FvMeXdQATMEkMhsxTjwubA.JtxDq5E76jNVON9238S9Br5q9Ohq', NULL, '2017-01-02 12:39:27', NULL, NULL),
(55, 'asd', 'sdq@grt.de', '$2y$10$tH6YyrJdNn2rKszQ5zBXOuOOG0iP1qA8eSeO.DUVSelvl6YVpCvXC', 'qNYTbIQwzu8jHM6LD6sfTKAJy4BCWvEWmYP0DVx7bpD0ZpEWlpwIJSfpbM3k', NULL, NULL, NULL),
(56, 'dfdfs', 'sdfsfs@g.fr', '$2y$10$CgcmVUcHSvdywFM74aQOde1Rw.VqGH9nJFA5R7Hm/mbud7Vi1r56i', 'SyfGqpvhDgozfhL6Xw1nb2KPIRX8moj5Aw43wHufZN0riOoiVPHDnKqbsHvr', NULL, NULL, NULL),
(57, 'jeremi', 'jeje.abt@yahoo.fr', '$2y$10$LvHWOHJ8pJE.9pzKzu9FyeZog5aDL/v6.CWtGbyUzV6LJNFEAmktK', 'i2b1uvpiBKJCdvUQOeCHloqZr5qdgU8x2sFUyYnkqsNNN6xYvvY6ONfibC3h', NULL, NULL, NULL),
(58, 'jejer', 'jeje.abt@yahoo.frd', '$2y$10$exUWbJnM7oCIr5sbbozwZeNRcYQMBJUwXv2DG1VT.X47wWncXS41O', NULL, '2017-01-07 23:39:00', NULL, NULL),
(59, 'jeje', 'eam@gm.fr', '$2y$10$ksUOIL0PzLuvN6sd7jpYa.wD2Bn486OpLLC6HUnPxNh4QJDlQJmWS', NULL, '2017-01-07 23:48:58', NULL, NULL),
(60, 'azeazeaze', 'zaeaze@grg.dfer', '$2y$10$aX/byDZcpbr5i3zI2seaZOGXpYNI.p2qx/ZSxNUjF9wH35xAL2i7G', NULL, '2017-01-07 23:50:29', NULL, NULL),
(61, 'sqdqdsqdqsdq', 'qsd@gf.ferzds', '$2y$10$7eEhkEnp5WFcqR37D8UokOnRrDLn/YF87esUK/Yqz/0lfHo7TG5ju', '4AMPcIzWs9BiHerwIb3XCHUj8WUwUyKu4xVFuquPq2IsEHXvZnbGtAfAAPHI', NULL, NULL, NULL),
(62, 'sqdqsdqsdqds', 'qsdqdsdq@gma.de', '$2y$10$ptcKeUaX7yNnpBSXCmzaXecL.Lw7DMbROQBadJQmKep5XoDLF9kHW', NULL, '2017-01-07 23:52:53', NULL, NULL),
(63, 'jeremiee', 'jeje.abt@y.fr', '$2y$10$G2NygOsqp1YAnIWdYH/ha.LyQvcllEf81xUuaPizPVk4tqlTPdVK2', NULL, '2017-01-07 23:54:16', NULL, NULL),
(64, 'jejejje', 'qsdqdq@grt.fr', '$2y$10$2qve98YIcLXWlIO.HknGCuTIogQiG7Qe3n.3KErq9GxFxaKZM3A2.', NULL, '2017-01-07 23:55:59', NULL, NULL),
(65, 'jeremie', 'abt@te.fr', '$2y$10$T8PwePwO.95eZ8uV8q.W.eYMZToQJmpBkRUE2HZVnuyTF6dzofS1G', NULL, '2017-01-19 12:36:45', NULL, NULL),
(66, 'ms', 'ms@gt.fr', '$2y$10$NSvNB1IRoDz67E0P6VxsSO1cxOBCa44NWpGn4aUJHYm7VFTWomp3i', NULL, '2017-01-19 12:41:10', NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
