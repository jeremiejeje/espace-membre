<?php require 'inc/header.php';  ?>
<?php 
if(!empty($_POST) && !empty($_POST['email'])){
	require_once 'inc/db.php';
	$req = $pdo->prepare('SELECT * FROM users WHERE email = :email AND confirmed_at IS NOT NULL');
	$req->execute(['email' => $_POST['email']]);
	$user = $req->fetch();
	if($user){
		$reset_token = str_random(60);
		$pdo->prepare('UPDATE users SET reset_token = ?, reset_at = NOW() WHERE id = ?')->execute([$reset_token, $user['id']]);
		$_SESSION['reset'] = $reset_token;
		$_SESSION['flash']['success'] = "les intructions du rappel de mot de passe vous ont été envoyées par emails";
		header('Location: login.php');die();
	}else{
		$_SESSION['flash']['danger'] = 'aucun compte ne correspond a cet addresse';
	}
}

 ?>


<h1>Mot de passe oublié</h1>

<form action="" method="POST">
	<div class="form-group">
		<label for="">Email</label>
		<input class="form-control" type="email" name="email"></input>
	</div>

	<button class="btn btn-primary" type='submit'>Se connecter</button>
</form>



<?php require 'inc/footer.php';  ?>