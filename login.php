<?php require 'inc/header.php';  ?>
<?php 
if(!empty($_POST['username']) && !empty($_POST['password'])){
	require_once 'inc/db.php';
	$req = $pdo->prepare('SELECT * FROM users WHERE (username = :username OR email = :username) AND confirmed_at IS NOT NULL');
	$req->execute(['username' => $_POST['username']]);
	$user = $req->fetch();
	if(password_verify($_POST['password'], $user['password'])){
		$_SESSION['auth'] = $user;
		$_SESSION['flash']['success'] = 'Vous êtes maintenan connecté';
		header('Location: account.php');die();
	}else{
		$_SESSION['flash']['danger'] = 'mot de passe ou identifiant incorrecte';
	}
}

 ?>


<h1>Se connecter</h1>

<form action="" method="POST">
	<div class="form-group">
		<label for="">Pseudo ou email</label>
		<input class="form-control" type="text" name="username"></input>
	</div>



	<div class="form-group">
		<label for="">mot de passe  <a href="forget.php">(J'ai oublié mon mot de passe)</a></label>
		<input class="form-control" type="password" name="password"></input>
	</div>

	

	<button class="btn btn-primary" type='submit'>Se connecter</button>
</form>



<?php require 'inc/footer.php';  ?>