<?php 
include 'inc/header.php';
require_once 'inc/db.php';		


$user_id = $_SESSION['id'];
$token = $_SESSION['token'];

$req = $pdo->prepare('SELECT confirmation_token, username, email, id FROM users WHERE id = ?');
$req->execute([$user_id]);

$user = $req->fetch();
$tokenGet = $_GET['token'];



if($tokenGet == $user['confirmation_token']){  
	$req = $pdo->prepare('UPDATE users SET confirmation_token = NULL, confirmed_at = NOW() WHERE id = ?')->execute([$user_id]);
	$_SESSION['flash']['success'] = 'votre compte à bien été validée';
	$_SESSION['auth'] = $user;
	header('Location: account.php');die();
}else{
  $_SESSION['flash']['danger'] = "ce token n'est plus valide ";
  header('Location: login.php');
}

