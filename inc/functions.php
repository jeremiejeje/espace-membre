<?php

function debug($var){
	echo '<pre>' . print_r($var, true) . '</pre>';
};


function str_random($length){
	$alphabet = "0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";
	return substr(str_shuffle(str_repeat($alphabet, $length)), 0, 60);
}

function logged(){
		if(!isset($_SESSION['auth'])){
		$_SESSION['flash']['danger'] = "vous n'avez pas le droit d'accéder à cette page";
		header('Location: login.php');die();

	}

}