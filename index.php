<?php  require 'inc/header.php'  ?>
<?php
//traitement du formulaire

if(!empty($_POST)){

	require_once 'inc/db.php';
	$errors = array();

	if(empty($_POST['username']) || !preg_match('#^[a-z0-9A-Z_]{0,}$#', $_POST['username'])){
		$errors['username'] = "votre pseudo n'est pas valide";

	}else{
		$req = $pdo->prepare("SELECT id FROM users WHERE username = ?");
		$req->execute([$_POST['username']]);
		if($req->fetch()){
			$errors['username'] = "votre pseudo est deja pris";
		}
	}

	if(empty($_POST['email']) || (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))){
		$errors['email'] = "votre champ n'est pas valide";
}else{
		$req = $pdo->prepare("SELECT id FROM users WHERE email = ?");
		$req->execute([$_POST['email']]);
		if($req->fetch()){
			$errors['email'] = "cette email est deja prise pour un autre compte";
		}
	}
	if(empty($_POST['password']) || ($_POST['password']) != ($_POST['password_confirm'])){
		$errors['password'] = 'vous devez rentrer un mot de passe valide';
	}

if(empty($errors)){
	
	$req = $pdo->prepare("INSERT INTO users SET username = ?, password = ?, email = ?, confirmation_token = ?");
	$password = password_hash($_POST['password'], PASSWORD_BCRYPT);
	$token = str_random(60);
	$req->execute([$_POST['username'], $password, $_POST['email'], $token]);
	$user_id = $pdo->lastInsertId();



	$email = $_POST['email'];
	$_SESSION['email'] = $email;

	$_SESSION['msg'] = "afin de valider votre compte merci de liquer sur ce lien :\n\n";
	$_SESSION['id'] = $user_id;
	$_SESSION['token'] = $token;
	$_SESSION['link'] = "confirm.php";
	$_SESSION['flash']['success'] = 'un email de confirmation vous a été, cette page représente le mail que vous auriez du recevoir, veuillez donc cliquer sur le lien pour valider votre inscription ;)';

	header('Location: confirm.php');die();
}


debug($errors);

}



?>

<?php if(!empty($errors)): ?>
	<div class="alert alert-danger">
		<p>vous n'avez pas rempli le formulaire correctement</p>
		<ul>
			<?php foreach($errors as $val): ?>
				<li><?= $val; ?> </li>

			<?php endforeach;?>
		</ul>

	</div>


<?php endif; ?>
	


<h1>S'inscrire</h1>

<form action="" method="POST">
	<div class="form-group">
		<label for="">Pseudo</label>
		<input class="form-control" type="text" name="username"></input>
	</div>


	<div class="form-group">
		<label for="">Email</label>
		<input class="form-control" type="text" name="email"></input>
	</div>

	<div class="form-group">
		<label for="">mot de passe</label>
		<input class="form-control" type="password" name="password"></input>
	</div>

	<div class="form-group">
		<label for="">Confirmez votre mot de passe</label>
		<input class="form-control" type="password" name="password_confirm"></input>
	</div>


	<button class="btn btn-primary" type='submit'>confirmer</button>
</form>


<?php require 'inc/footer.php'  ?>