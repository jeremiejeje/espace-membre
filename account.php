<?php 
require 'inc/header.php' ;
logged();

if(!empty($_POST)){
	if(empty($_POST['password']) || $_POST['password'] != $_POST['password_confirm']){
		$_SESSION['flash']['danger'] = "les mots de passes ne correspondent pas";

	}else{
		$user_id = $_SESSION['auth']['id'];
		$password = password_hash($_POST['password'], PASSWORD_BCRYPT);
		require_once 'inc/db.php';
		$pdo->prepare('UPDATE users SET password = ? WhERE id = ?')->execute([$password, $user_id]);
		$_SESSION['flash']['success'] = "votre mot de passe a bien été mis à jour"; 

	}


}



?>



<h1>votre compte</h1>

<form action="" method="POST">
	<div class="form-group">
		<input class="form-control" type="password" name="password" placeholder="changer de mot de passe" />
	</div>

	<div class="form-group">
		<input class="form-control" type="password" name="password_confirm" placeholder="confirmation du mot de passe" />
	</div>
	<button class="btn btn-primary" sqtype='submit'>confirmation</button>

</form>

<?php require 'inc/footer.php' ?>